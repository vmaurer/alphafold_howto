Run AlphaFold monomer (original DeepMind's version)
===================================================

1. Copy Slurm sbatch script ``run_AF_monomer.sh``
2. Adjust the name of the FASTA file with a single sequence
3. Adjust job submission options if necessary
4. Run ``sbatch run_AF_monomer.sh``

    .. tip:: 
        
        ``--fasta_paths=`` option in the ``run_AF_monomer.sh`` script can point to a comma separated list of FASTA files of multiple proteins.
        I haven't used it though so do not know whether they will be run in parallel or consecutively. 
        If the consecutively, probably you want to submit the jobs as separate commands to run in parallel.

Run AlphaFold multimer (original DeepMind's version)
====================================================

1. Prepare FASTA file with with sequences of the multimer

    The FASTA file needs to contain all sequences for a complex like the following for a heterodimer::

        >seq1
        XXXXXXX
        >seq2
        XXXXXXX

    For homo-oligomers, just repeat the same sequence multiple times, e.g. for a trimer::

        >seq1
        XXXXXXX
        >seq1
        XXXXXXX
        >seq1
        XXXXXXX

2. Copy Slurm sbatch script ``run_AF_multimer.sh``
3. Adjust the name of the FASTA file 
4. Adjust job submission options if necessary
5. Run ``sbatch run_AF_monomer.sh``

Run local ColabFold monomer or multimer
=======================================

1. Install:

    .. code-block:: bash
        
        module load Anaconda3
        conda create --name ColabFold
        source activate ColabFold

        #Below instructions directly from https://github.com/sokrypton/ColabFold
        pip install "colabfold[alphafold] @ git+https://github.com/sokrypton/ColabFold"
        pip install -q "jax[cuda]>=0.3.8,<0.4" -f https://storage.googleapis.com/jax-releases/jax_cuda_releases.html
        # For template-based predictions also install kalign and hhsuite
        conda install -c conda-forge -c bioconda kalign2=2.04 hhsuite=3.3.0
        # For amber also install openmm and pdbfixer
        conda install -c conda-forge openmm=7.5.1 pdbfixer

    Set how Matplotlib will render images:

    Open (or create your matplotlibrc) file that might be:

    .. code-block:: bash

        ~/.config/matplotlib/matplotlibrc

    and add a line like this:

    .. code-block:: bash

        backend      : Agg

2. Create a new directory for your job
3. Prepare FASTA file with a single sequence. For multimer, concatenate sequences of the different proteins separated by a semicolon ":", just as with usual ColabFold
4. Copy the ``sbatch`` template ``run_ColabFold.sh``
5. Adjust the name of the FASTA file 
6. Adjust job submission options if necessary
7. Adjust ``colabfold_batch`` options

    .. tip::

        Use ``colabfold_batch --help`` to see all options.

    .. tip::

        To run the old ColabFold for complexes, equivalent to the original https://colab.research.google.com/github/sokrypton/ColabFold/blob/main/beta/AlphaFold2_advanced.ipynb,
        add ``--model-type AlphaFold2-ptm`` option to the ``colabfold_batch`` command.
        This version is less accurate and less sensitive, but does not suffer from the steric clash bug.

8. Run ``sbatch run_ColabFold.sh``


    .. warning::

        Do not run too many ColabFold jobs in parallel. It uses an external public MMSQE2 server for generating alignments,
        so it may overload that server, perhaps even blocking our IP address.

Using custom templates with ColabFold
-------------------------------------
    .. warning::

        ColabFold can use templates for either monomer or multimer modes, but does not use the relative orientation of chains in the multimeric templates.


1. Create a directory for your templates
2. In this directory place your templates so they have:
    - mmCIF format
    - have only four characters in the filename, like: 1xxx.cif
    - have a release date records like this::
    
        #
        loop_
        _pdbx_audit_revision_history.data_content_type
        _pdbx_audit_revision_history.major_revision
        _pdbx_audit_revision_history.minor_revision
        _pdbx_audit_revision_history.revision_date
        'Structure model' 1 0 2100-01-01
        #
3. Add the ``--custom-template-path <your templates directory>`` option to the ``colabfold_batch`` command, and run the same way.

Run old local ColabFold multimer
================================

This is rather not useful anymore, kept for backward compatibility. 

Install and run following instructions from: https://github.com/jkosinski/alphafold/blob/sokrypton_alphafold2_advanced/README.md
 
Run FoldDock
============

Run OpenFold
============

Run RoseTTAfold 
===============
## To run RoseTTAFold2 at HD cluster:

1. Go to RF2 working directory:
`cd /g/kosinski/dima/RF2/RoseTTAFold2`

2. Create conda environment using `RF_min.yml` file:
`conda env create -f RF2-linux.min.yml`
The full package is summarized at RF2-linux.full.yml for your reference.

3. Activate conda environment:
`conda activate RF2`

4. You also need to install NVIDIA's SE(3)-Transformer (**please use SE3Transformer in this repo to install**).
 `cd SE3Transformer`
 `pip install --no-cache-dir -r requirements.txt`
 `python setup.py install`
 **You may want to compile dgl library from source code to get half precision support (recommended to handle protein > 1000 aa)**
  
5. Download and install third-party software:
 `./install_dependencies.sh`

6. Download the script to your Projectdir from here (https://git.embl.de/grp-kosinski/alphafold_howto)

7. Edit PATH_TO_RF2NA and point to your installation path <- TODO: make PATH_TO_RF2NA argument to the sbatch script

8. Run it:
`cd Projectdir`
`sbatch -J job_name run_RF2_monomer.sh input.fasta /PATH/TO/OUTPUT/DIRECTORY/`


Run RoseTTAFold2NA
===============

1. Go to RF2 working directory:
`cd /g/kosinski/dima/RF2/RoseTTAFold2NA`

2. Create conda environment using `RF2na-linux.yml` file:
`conda env create -f RF2na-linux.yml`

3. Activate conda environment:
`conda activate RF2NA`

4. You also need to install NVIDIA's SE(3)-Transformer (**please use SE3Transformer in this repo to install**).
 `cd SE3Transformer`
 `pip install --no-cache-dir -r requirements.txt`
 `python setup.py install`

5. Download the script to your Projectdir from here (https://git.embl.de/grp-kosinski/alphafold_howto) and run it:
`cd Projectdir`
`sbatch -J job_name run_RF2NA_slurm.sh /PATH/TO/OUTPUT/DIRECTORY/ P:protein.fa R:DNA.fa D:DNA.fa`
here P stands for proteins, D for DNA and R for RNA
the script takes any number of fasta files. 

Locations of databases for RoseTTAFold2(NA) is at:
`/scratch/RoseTTAFold_DBs`

The weights are pre-downloaded to 
`/g/kosinski/dima/RoseTTAFold2*/network/weights`


ColabFold setup for development
===============================

It is weird, yes. See more at:
https://github.com/sokrypton/ColabFold/blob/main/Contributing.md

1. Install

.. code-block:: bash

    module load AlphaFold
    module load GCC/10.2.0
    module load tqdm
    module load matplotlib

    DEVEL_DIR=<your dir>
    cd $DEVEL_DIR
    git clone https://github.com/steineggerlab/alphafold.git AlphaFold_ColabFold
    git clone https://github.com/jkosinski/ColabFold.git
    mkdir site-packages
    pip install "colabfold[alphafold] @ git+https://github.com/sokrypton/ColabFold" \
        --no-deps \
        --target=site-packages
    pip install alphafold-colabfold --no-deps --target=site-packages
    rm -r site-packages/colabfold
    ln -s $DEVEL_DIR/ColabFold/colabfold site-packages/colabfold
    rm -r site-packages/alphafold
    ln -s $DEVEL_DIR/AlphaFold_ColabFold/alphafold site-packages/alphafold

2. Use:

.. code-block:: bash

    module load AlphaFold
    module load GCC/10.2.0
    module load tqdm
    module load matplotlib
    export PYTHONPATH=$DEVEL_DIR/site-packages:$PYTHONPATH
    export PYTHONPATH=$DEVEL_DIR/site-packages/alphafold:$PYTHONPATH

