#!/bin/bash

#A typical runs takes couple of hours but may be much longer
#SBATCH --time=2-00:00:00

#log files:
#SBATCH -e AF_%x_err.txt
#SBATCH -o AF_%x_out.txt

#qos sets priority, you can set to high or highest but there is a limit of high priority jobs per user: https://wiki.embl.de/cluster/Slurm#QoS
#SBATCH --qos=normal

#Run a single task job on A100 gpu node. Use A100 only for long sequences. Other nodes: https://wiki.embl.de/cluster/Hardware. 
#If you don't care about the specific gpu card type, just delete the #SBATCH -C gpu=A100 line.
#SBATCH -p gpu-el8
#SBATCH -C gpu=A100

#Reserve the entire GPU so no-one else slows you down
#SBATCH --gres=gpu:1

#Limit the run to a single node
#SBATCH -N 1

#Adjust this depending on the node
#Here for a node with A100 on the HD cluster, which have 32 cpus and total 512 GB RAM
#SBATCH --ntasks=32
#SBATCH --mem=512000



#module load AlphaFold
module load AlphaFold/2.2.3-foss-2021a-CUDA-11.3.1

export ALPHAFOLD_JACKHMMER_N_CPU=$SLURM_NTASKS
export ALPHAFOLD_HHBLITS_N_CPU=$SLURM_NTASKS

# If you use --cpus-per-task=X and --ntasks=1 your script should contain:
# export ALPHAFOLD_JACKHMMER_N_CPU=$SLURM_CPUS_PER_TASK
# export ALPHAFOLD_HHBLITS_N_CPU=$SLURM_CPUS_PER_TASK

# TF_FORCE_UNIFIED_MEMORY='1' XLA_PYTHON_CLIENT_MEM_FRACTION are optional but may be necessary for bigger sequences.
export TF_FORCE_UNIFIED_MEMORY='1'
MAXRAM=$(echo `ulimit -m` '/ 1024.0'|bc)
GPUMEM=`nvidia-smi --query-gpu=memory.total --format=csv,noheader,nounits|tail -1`
export XLA_PYTHON_CLIENT_MEM_FRACTION=`echo "scale=3;$MAXRAM / $GPUMEM"|bc`

echo 'MAXRAM:' $MAXRAM
echo 'GPUMEM:' $GPUMEM
echo 'XLA_PYTHON_CLIENT_MEM_FRACTION:' $XLA_PYTHON_CLIENT_MEM_FRACTION

#If you read this after 2050-01-01, probably you want to adjust the date
time alphafold --fasta_paths=your_fasta.fasta --output_dir=./ --model_preset=monomer_ptm --max_template_date=2050-01-01 
