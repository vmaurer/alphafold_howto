########################################################
# SLURM script that allows modifying AlphaFold options #
# using sbatch options.                                #
########################################################

#!/bin/bash
#SBATCH -p gpu-el8
#SBATCH -N 1      # do not distribute on multiple nodes
#SBATCH -n 8
#SBATCH --mem=60GB
#SBATCH -C 'gpu=3090|gpu=2080Ti'
#SBATCH -t 0-12:0:0
#SBATCH --mail-type=ALL
#SBATCH --gres=gpu:1
#SBATCH -e %x-%j.err
#SBATCH -o %x-%j.out

#example1 (run for max 12 hours on either 3090 gpu or 2080Ti GPU with 8 cores)
#  cd Projectdir
#  sbatch -J a_useful_job_name   run_af2.1.1.sh    input.fa results 2050-01-01 full_dbs multimer 
#
#example2 (run on full A100 node for 1 1/2 day)
#  cd Projectdir
#  sbatch  -J a_useful_job_name -n32 -C gpu=A100 --mem=500GB -t 1-12:0:0   run_af2.1.1.sh   input.fa results 2050-01-01 full_dbs multimer
#
#example3 (for multimer, specify that the complex is from prokaryotes for sequence pairing)
#  cd Projectdir
#  sbatch -J a_useful_job_name   run_af2.1.1.sh    input.fa results 2050-01-01 full_dbs multimer '--is_prokaryote_list=true'
#


GANGLIASTARTTIME=`date "+%m%%2F%d%%2F%Y+%H%%3A%M"`
GANGLIAMACHINE=`hostname -s|awk '{print toupper($1)}'`
GANGLIAGPUS=`echo $CUDA_VISIBLE_DEVICES|sed -e "s/,/%2C/g"`
STARTURLGPU='http://web.cluster.embl.de/ganglia/graph.php?r=hour&z=xlarge&title=GPU_util&vl=&x=&n=&hreg[]='`hostname`'&mreg[]=gpu['$GANGLIAGPUS']_util&gtype=line&glegend=show&aggregate=1&embed=1&cs='$GANGLIASTARTTIME

echo "Your Job $SLURM_JOB_NAME $SLURM_JOBID is running on node `hostname -s` using the GPU device(s) $CUDA_VISIBLE_DEVICES"; echo "Please monitor the GPU utilization on: $STARTURLGPU" 

FASTA=$1
OUTPUT=$2
#TODO: set defaults:
MAXT=$3
DB_PRESET=$4
MODEL_PRESET=$5
OTHER=$6
#MAXT=2050-01-01

mkdir $OUTPUT
module load AlphaFold
module list
export ALPHAFOLD_JACKHMMER_N_CPU=$SLURM_NTASKS
export ALPHAFOLD_HHBLITS_N_CPU=$SLURM_NTASKS 

# TF_FORCE_UNIFIED_MEMORY='1' XLA_PYTHON_CLIENT_MEM_FRACTION are optional but may be necessary for bigger sequences.
export TF_FORCE_UNIFIED_MEMORY='1'
MAXRAM=$(echo `ulimit -m` '/ 1024.0'|bc)
GPUMEM=`nvidia-smi --query-gpu=memory.total --format=csv,noheader,nounits|tail -1`
export XLA_PYTHON_CLIENT_MEM_FRACTION=`echo "scale=3;$MAXRAM / $GPUMEM"|bc`

echo 'MAXRAM:' $MAXRAM
echo 'GPUMEM:' $GPUMEM
echo 'XLA_PYTHON_CLIENT_MEM_FRACTION:' $XLA_PYTHON_CLIENT_MEM_FRACTION


echo running command: /usr/bin/time -v run_alphafold.py --fasta_paths=$FASTA --output_dir=$OUTPUT --max_template_date=$MAXT  --db_preset=$DB_PRESET --model_preset=$MODEL_PRESET --benchmark=False --logtostderr $OTHER
/usr/bin/time -v run_alphafold.py --fasta_paths=$FASTA --output_dir=$OUTPUT --max_template_date=$MAXT  --db_preset=$DB_PRESET --model_preset=$MODEL_PRESET --benchmark=False --logtostderr $OTHER

GANGLIAENDTIME=`date "+%m%%2F%d%%2F%Y+%H%%3A%M"`
echo "Your Job $SLURM_JOB_NAME $SLURM_JOBID was running on node `hostname -s` using the GPU device(s) $CUDA_VISIBLE_DEVICES."; echo "GPU utilization record: $STARTURLGPU""&ce=""$GANGLIAENDTIME"
 
