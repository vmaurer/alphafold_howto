#!/bin/bash

#A typical runs takes couple of hours but may be much longer
#SBATCH --time=2-00:00:00

#log files:
#SBATCH -e AF_%x_err.txt
#SBATCH -o AF_%x_out.txt

#qos sets priority, you can set to high or highest but there is a limit of high priority jobs per user: https://wiki.embl.de/cluster/Slurm#QoS
#SBATCH --qos=normal

#Limit the run to a single node
#SBATCH -N 1

#SBATCH --ntasks=8
#SBATCH --mem=60000



module load AlphaFold/2.1.1-fosscuda-2020b-interruptible

export ALPHAFOLD_JACKHMMER_N_CPU=$SLURM_NTASKS
export ALPHAFOLD_HHBLITS_N_CPU=$SLURM_NTASKS

# If you use --cpus-per-task=X and --ntasks=1 your script should contain:
# export ALPHAFOLD_JACKHMMER_N_CPU=$SLURM_CPUS_PER_TASK
# export ALPHAFOLD_HHBLITS_N_CPU=$SLURM_CPUS_PER_TASK

time run_alphafold.py --fasta_paths=your_fasta.fasta --output_dir=./ --model_preset=monomer --max_template_date=2050-01-01 --break_after 1 
